#pragma once

#include <string>
#include <vector>
#include "Direct3D.h"
#include "Transform.h"
#include "Fbx.h"



namespace Model
{
	struct ModelData
	{
		Transform transform;
		std::string fileName;
		SHADER_TYPE shaderType;
		Fbx* pFbx;

		ModelData() : pFbx(nullptr)
		{
		}
	};

	int Load(std::string fileName, SHADER_TYPE shaderType = SHADER_TEST);
	void SetTransform(int handle, Transform& transform);
	void Draw(int handle);

	void RayCast(int handle, RayCastData *rayData);


//任意のボーンの位置を取得
	//引数：handle		調べたいモデルの番号
	//引数：boneName	調べたいボーンの名前
	//戻値：ボーンの位置（ワールド座標）
XMVECTOR GetBonePosition(int handle, std::string boneName);

};