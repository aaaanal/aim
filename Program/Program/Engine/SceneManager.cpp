#include "SceneManager.h"
#include "../TestScene.h"

SceneManager::SceneManager()
{
}

SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager"),
	nowScene_(SCENE_ID_PLAY), nextScene_(SCENE_ID_PLAY)
{
}

SceneManager::~SceneManager()
{
}

void SceneManager::Initialize()
{
	Instantiate<TestScene>(this);
}

void SceneManager::Update()
{
	if (nowScene_ != nextScene_)
	{
		AllChildKill();
		switch (nextScene_)
		{
		case SCENE_ID_TEST: Instantiate<TestScene>(this); break;
		}

		nowScene_ = nextScene_;
	}
}

void SceneManager::Draw()
{
}

void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID scene)
{
	nextScene_ = scene;
}
