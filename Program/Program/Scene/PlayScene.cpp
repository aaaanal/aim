#include "PlayScene.h"
#include "../Object/Player.h"
#include "../Object/Target.h"
#include "../Object/Danger.h"
#include "../Object/Scope.h"

//コンストラクタ
PlayScene::PlayScene(GameObject* parent)
	: GameObject(parent, "PlayScene") {
}

//初期化
void PlayScene::Initialize() {

    Instantiate<Player>(this);
    Instantiate<Target>(this);

}

//更新
void PlayScene::Update() {


    //もし世界にターゲットが見つからなかったら
    if (FindObject() != 0) {

        //ターゲットを5つ設置します
        for (int i = 0; i <= 5; i++) {
            Instantiate<Target>(this);
        }
    }
}

//描画
void PlayScene::Draw() {
}

//開放
void PlayScene::Release() {
}