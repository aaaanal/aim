#pragma once
#include "../Engine/GameObject.h"

//◆◆◆を管理するクラス
class Target : public GameObject
{
    int hModel_;    //モデル番号
public:
    //コンストラクタ
    Target(GameObject* parent);

    //デストラクタ
    ~Target();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;
};