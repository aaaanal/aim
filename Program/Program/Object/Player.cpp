#include "Player.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/Model.h"
#include "Bullet.h"


/*こいつができること
* 
* ・マウスでのカメラ移動
* ・左クリックで弾を飛ばす
* 
*/

//コンストラクタ
Player::Player(GameObject* parent)
    :GameObject(parent, "Player") {
}

//デストラクタ
Player::~Player() {
}

//初期化
void Player::Initialize() {

}

//更新
void Player::Update() {

    //カメラポジションにマウスポジションを直入れ
    Camera::SetTarget(Input::GetMousePosition());

    //カメラポジションにプレイヤーポジションを直入れ
    //Camera::SetPosition();


    //左クリックで弾を出す
    if (Input::IsMouseButtonDown(0)) {
       // Instantiate<Bullet>(this->pParent_);
        Bullet* pBullet = Instantiate<Bullet>(FindObject("PlayScene"));

        XMVECTOR shotPos = Model::GetBonePosition(this, "ShotPoint");
        XMVECTOR cannonRoot = Model::GetBonePosition(this, "CannonRoot");

        pBullet->Shot(shotPos, shotPos - cannonRoot);
    }
}

//描画
void Player::Draw() {
}

//開放
void Player::Release() {
}

//設定で切り替えたのを入れる
//おしっぱでしゃがみか一回押したらしゃがみか
//bool crouch_down(bool bo) {
//
//    //そのキーを押したらしゃがむ
//    if (bo = true) {
//        if (Input::IsKey(DIK_SPACE)) {
//            
//        }
//    }
//
//
//
//
//}