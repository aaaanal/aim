#pragma once
#include "../Engine/GameObject.h"

//◆◆◆を管理するクラス
class Danger : public GameObject
{

public:
    //コンストラクタ
    Danger(GameObject* parent);

    //デストラクタ
    ~Danger();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;
};