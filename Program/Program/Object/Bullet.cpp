#include "Bullet.h"
#include "../Engine/Model.h"
#include "../Engine/Collider.h"

/*こいつができること
*
* ・自身のモデルデータのロード
* ・前進
*
* 以上
*/

//コンストラクタ
Bullet::Bullet(GameObject* parent)
    :GameObject(parent, "Bullet"), hModel_(-1), SPEED(3.0f) {

}

//デストラクタ
Bullet::~Bullet() {

}

//初期化
void Bullet::Initialize() {
    //モデルデータのロード
    hModel_ = Model::Load(D_MODEL + "BulletModel" + FBX);
    assert(hModel_ >= 0);

}

//更新
void Bullet::Update() {

    transform_.position_ += move_;
    move_.vecY -= 0.02f;

}

//描画
void Bullet::Draw() {

}

//開放
void Bullet::Release() {

}

//発射
void Bullet::Shot(XMVECTOR position, XMVECTOR direction) {
    //位置
    transform_.position_ = position;

    move_ = XMVector3Normalize(direction) * SPEED;
}