#include "Target.h"
#include "../Engine/Model.h"

/*こいつができること
* 
* ・自身のモデルデータのロード
* 
* 以上
*/

//コンストラクタ
Target::Target(GameObject* parent)
    :GameObject(parent, "Target"), hModel_(-1) {

}

//デストラクタ
Target::~Target() {

}

//初期化
void Target::Initialize() {
    //モデルデータのロード
    hModel_ = Model::Load(D_MODEL + "TargetModel" + FBX);
    assert(hModel_ >= 0);

}

//更新
void Target::Update() {


  
}

//描画
void Target::Draw() {

}

//開放
void Target::Release() {

}