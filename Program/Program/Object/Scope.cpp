#include "Scope.h"

//コンストラクタ
Scope::Scope(GameObject* parent)
    :GameObject(parent, "Scope") {
}

//デストラクタ
Scope::~Scope() {
}

//初期化
void Scope::Initialize() {
}

//更新
void Scope::Update() {
}

//描画
void Scope::Draw() {
}

//開放
void Scope::Release() {
}