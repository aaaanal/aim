#include "Danger.h"

//コンストラクタ
Danger::Danger(GameObject* parent)
    :GameObject(parent, "Danger") {
}

//デストラクタ
Danger::~Danger() {
}

//初期化
void Danger::Initialize() {
}

//更新
void Danger::Update() {
}

//描画
void Danger::Draw() {
}

//開放
void Danger::Release() {
}