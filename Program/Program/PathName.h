#pragma once
#include <string>

namespace PathName {

	//ディレクトリ--------------------------------------
	const std::string D_IMAGE		= "Assets\\Image\\";
	const std::string D_MODEL		= "Assets\\Model\\";
	const std::string D_SOUND		= "Assets\\Sound\\";
	const std::string D_CSV			= "Assets\\CsvFile\\";
	//--------------------------------------------------

	//拡張子--------------------------------------------

		//イラスト
	const std::string PNG	= ".png";
	const std::string JPEG	= ".jpg";
	const std::string BMP	= ".bmp";

		//モデル
	const std::string FBX	= ".fbx";

		//音楽
	const std::string WAV	= ".wav";

		//文字データ
	const std::string TXT	= ".txt";
	const std::string CSV	= ".csv";

	//--------------------------------------------------

}